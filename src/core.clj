(ns core
  (:require [clojure.java.io :as io]))

(defn generate-attribute [a]
  (format "    %s sub attribute,\n        value %s;"
          a
          "string"))

(defn generate-entity [{:keys [table columns]}]
  (apply str (concat (->> (concat [(format "    %s sub entity" table)]
                                  (mapv #(format "        owns %s" %) columns))
                          (interpose ",\n"))
                     [";"])))

(defn get-schema [f]
  (with-open [r (io/reader f)]
    (println (format "  %s" f))
    {:table   (clojure.string/replace (.getName f) ".csv" "")
     :columns (clojure.string/split (doall (first (line-seq r))) #",")}))

(defn main [{:keys [path output]}]
  (let [files (->> (file-seq (io/file path))
                   (filter #(clojure.string/ends-with? (.getName %) ".csv"))
                   (sort))]
    (println "Generating schemas for TypeDB")
    (println (format "Loading CSV files from: %s" path))
    (let [schema (mapv get-schema files)
          attributes (->> schema
                          (map :columns)
                          (flatten)
                          (set)
                          (sort)
                          (mapv generate-attribute))
          entities (mapv generate-entity schema)
          final (apply str (interpose "\n\n"
                                      (concat ["define"]
                                              attributes
                                              entities)))]
      (spit output final))
    #_(println (str "  "
                    (->> files
                         (interpose "\n  ")
                         (apply str))))))

(defn keywordize-param [s]
  "Checks to see if param starts with '--' and then converts to keyword.

  Throws IllegalArgumentException otherwise"
  (if (clojure.string/starts-with? s "--")
    (keyword (apply str (drop 2 s)))
    (throw (IllegalArgumentException. (format "Parameters must start with '--' but got '%s'" s)))))

(defn -main [& opts]
  "Wrapper converts --params into map with keywords and passes onto main"
  (let [args (into {} (mapv (fn [[k v]] [(keywordize-param k) v]) (partition 2 opts)))]
    (println "args:" args)
    (main args)))