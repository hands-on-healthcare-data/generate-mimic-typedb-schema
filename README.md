# generate-mimic-typedb-schema

_**You should not need to run this code. It's provided for reference but the TypeDB schema has already been created for
you.**_

This is a simple helper library that autogenerates a TypeDB schema for the MIMIC-III dataset.

Simply, it does the following:

1. Takes a directory containing all the MIMIC-III CSV files as input
2. Extracts the table name from the filename
3. Extracts the columns from the first row of each file
4. Generate attributes for each column as a string
5. Generate entities for each table, owning all the columns

This is very simple generator to minimize the tedious part of translating the CSV headers to a TypeDB schema.

After a file is generated, you will want to:

1. Update the datatypes for fields that are not strings (e.g., date, long, ...)
2. Some entities should be relations
3. Entities need to play certain roles

## Usage

`clj -M -m core --path ~/mimic-iii-clinical-database-demo-1.4 --output mimic-schema-template.tql`